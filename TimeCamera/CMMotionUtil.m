//
//  CMMotionUtil.m
//  FlightLogger
//
//  Created by 桑原宜昭 on 2014/09/13.
//  Copyright (c) 2014年 桑原宜昭. All rights reserved.
//

#import "CMMotionUtil.h"

@implementation CMMotionUtil
@synthesize ud;
@synthesize motionManager;
@synthesize systemVersion;
@synthesize defaultPitch;
@synthesize defaultRoll;
@synthesize defaultYaw;
@synthesize pitch;
@synthesize roll;
@synthesize yaw;
@synthesize isResetAttitude;

// 初期化
- (id)initWithDelegate:(id <CMMotionUtilDelegate>)delegate
{
    self = [super init];
    
    if(self) self.delegate = delegate;
    
    return self;
}

// CMMotionのセットアップ
- (void)setupCMMotion
{
    // ユーザーデフォルト初期化
    ud = [NSUserDefaults standardUserDefaults];
    
    // iOSVersionの取得
    systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];

    // CMMotionの初期化
    if (motionManager == nil)  motionManager = [[CMMotionManager alloc] init];
    
    // CMMotionの初期姿勢値
    defaultPitch = 0.0f;
    defaultRoll  = 0.0f;
    defaultYaw   = 0.0f;
    
    // CMMotionの初期化用フラグ
    isResetAttitude = NO;
    
    // CMMotionが有効ならば値の取得開始
    if (motionManager.deviceMotionAvailable) [self startCMMotion];
}

// CMMotionの開始
- (void)startCMMotion
{
    // CMMotionの更新頻度
    motionManager.deviceMotionUpdateInterval = 0.00833333;
    
    __block NSInteger count = 0;
    __block float totalUserAcceleration = 0.0;
    
    void (^handler)(CMDeviceMotion *, NSError *) = ^(CMDeviceMotion *motion, NSError *error) {
        
        pitch = motion.attitude.pitch * 180 / M_PI - defaultPitch;
        roll  = motion.attitude.roll  * 180 / M_PI - defaultRoll;
        yaw   = motion.attitude.yaw   * 180 / M_PI - defaultYaw;
        
        // 初期姿勢値の設定
        if (isResetAttitude) {
            
            defaultPitch  = motion.attitude.pitch * 180 / M_PI;
            defaultRoll   = motion.attitude.roll  * 180 / M_PI;
            isResetAttitude = NO;
        }
        
        // - 90 : 360
        //    0 : 270
        //   90 : 180
        
        while (360.0 < pitch) pitch -= 360.0;
        while (pitch < 0.0)   pitch += 360.0;
        while (360.0 < roll)  roll  -= 360.0;
        while (roll < 0.0)    roll  += 360.0;
        while (360.0 < yaw)   yaw   -= 360.0;
        while (yaw < 0.0)     yaw   += 360.0;
        
        if (270 < roll && roll < 360) roll -= 360;
        
        //NSLog(@"pitch:%.2f roll:%.2f yaw:%.2f", pitch, roll, yaw);
        
        [self.delegate updateAttitude:pitch :roll :yaw];
        
        totalUserAcceleration += motion.userAcceleration.z;
    };
    
    // CMMotionの開始
    // Z軸を鉛直として、X軸を横とし、電子コンパスで位置を修正する
    if (5.0 < systemVersion) [motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryCorrectedZVertical toQueue:[NSOperationQueue currentQueue] withHandler:handler];
    // 調整できるのはiOS5以降
    else                     [motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:handler];
}

// CMMotionの停止
- (void)stopCMMotion
{
    // CMMotionの停止
    if (motionManager.deviceMotionActive) [motionManager stopDeviceMotionUpdates];
}

// 初期姿勢値の設定
- (void)resetAttitude
{
    // CMMotionにデバイスが対応していない場合
    if (!motionManager.deviceMotionAvailable) return;
    
    // CMMotionが開始していない場合
    if (!motionManager.deviceMotionActive) [self startCMMotion];
    
    defaultPitch = 0.0f;
    defaultRoll  = 0.0f;
    
    isResetAttitude = YES;
}

// 姿勢値の設定
- (void)setAttitude:(float)_pitch :(float)_roll :(float)_yaw
{
    if (0 < _pitch) defaultPitch = _pitch;
    if (0 < _roll)  defaultRoll  = _roll;
    if (0 < _yaw)   defaultYaw   = _yaw;
}

@end
