//
//  ViewController.swift
//  TimeCamera
//
//  Created by 石郷 祐介 on 2015/05/23.
//  Copyright (c) 2015年 Yusk. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class ViewController: UIViewController, CMMotionUtilDelegate, GMapCaptureDelegate
{
	private var camera:CamCapture?							// カメラ管理
	private var previewLayer:CALayer?						// プレビューレイヤ
	private var motionUtil:CMMotionUtil?					// モーション管理
	private var gmapCapture:GMapCapture = GMapCapture()		// Googleストリートビュー管理
	private var previewImgView:UIImageView?					// プレビュー画像
	
	private var shootAudioPlayer:AVAudioPlayer?
	
	@IBOutlet private weak var shootBtn:UIButton?			// 撮影ボタン
	@IBOutlet private weak var closeBtn:UIButton?			// プレビュー画面閉じるボタン
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		self.gmapCapture.delegate = self
		
		// シャッター音
		if let path = NSBundle.mainBundle().pathForResource("ShootSE", ofType: "mp3")
		{
			let url = NSURL(fileURLWithPath: path)
			self.shootAudioPlayer = try? AVAudioPlayer(contentsOfURL: url)
		}
		
		// カメラ初期化
		self.setupCamera(CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height))
	
		// 位置情報サービス開始
		let locationUtil = LocationUtil.sharedInstance
		locationUtil.startLocationUpdating()
		
		// モーションサービス開始
		self.motionUtil = CMMotionUtil(delegate: self)
		self.motionUtil?.setupCMMotion()

		// 撮影ボタン位置調整
		if let shootBtn = self.shootBtn
		{
			let shootBtnSize = CGSize(width: 55.0, height: 55.0)
			let shootBtnMarginSize = CGSize(width: 10.0, height: 0.0)
			shootBtn.frame.origin = CGPoint(
				x: self.view.frame.size.width - shootBtnSize.width - shootBtnMarginSize.width,
				y: self.view.frame.size.height/2 - shootBtnSize.height/2)
			self.view.bringSubviewToFront(shootBtn)
		}

		// 閉じるボタン位置調整
		if let closeBtn = self.closeBtn
		{
			let closeBtnSize = CGSize(width: 50.0, height: 50.0)
			closeBtn.frame.origin = CGPoint(
				x: self.view.frame.size.width - closeBtnSize.width - 10.0,
				y: 10.0)
		}
	}
	
	/*
	 * カメラ初期化
	*/
	private func setupCamera(cameraSize:CGSize)
	{
		self.camera = CamCapture()
		
		self.previewLayer = self.camera?.previewLayerWithFrame(
			CGRect(x: 0.0, y: 0.0, width: cameraSize.width, height: cameraSize.height))
		self.view.layer.addSublayer(self.previewLayer)
	}
	
	/*
	 * 撮影
	*/
	@IBAction func shoot()
	{
		let locationUtil = LocationUtil.sharedInstance
		
		if let motionUtil = self.motionUtil
		{
			self.closeBtn?.enabled = true
			self.shootBtn?.enabled = false

			self.shootAudioPlayer?.play()
			
			self.gmapCapture.capture(
				locationUtil.latitude,
				longitude: locationUtil.longitude,
				heading: locationUtil.heading + 90.0,
				pitch: Double((motionUtil.roll - 90.0 + 180.0) * -1.0))
//			self.gmapCapture.capture(
//				locationUtil.latitude,
//				longitude: locationUtil.longitude,
//				heading: locationUtil.heading - 90.0,
//				pitch: 0.0)

		}
	}
	
	/*
	 * プレビュー画面を閉じる
	*/
	@IBAction func closePreview()
	{
		self.closeBtn?.alpha = 0.0
		self.closeBtn?.enabled = false
		
		self.shootBtn?.enabled = true
		
		UIView.animateWithDuration(0.7,
			animations: { () -> Void in
				self.previewImgView?.alpha = 0.0
			}, completion: { (finished:Bool) -> Void in
				self.previewImgView?.removeFromSuperview()
				self.previewImgView = nil
			})
	}
	
	// MARK: - GMapCapture Delegate methods
	
	func GMapCaptureDidCapture(gmapCapture: GMapCapture, image: UIImage)
	{
		if let previewImgView = self.previewImgView
		{
			let nextImgView = UIImageView(frame: CGRect(
				x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height))
			nextImgView.image = image
			nextImgView.alpha = 0.0
			self.view.addSubview(nextImgView)
			
			UIView.animateWithDuration(0.7,
				animations: { () -> Void in
					self.previewImgView?.alpha = 0.0
					nextImgView.alpha = 1.0
				},
				completion: { (finished:Bool) -> Void in
					if (finished)
					{
						self.previewImgView?.removeFromSuperview()
						self.previewImgView = nextImgView
					}
				})
		}
		else
		{
			self.previewImgView = UIImageView(frame: CGRect(
				x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height))
			self.previewImgView?.image = image
			self.previewImgView?.alpha = 0.0
			self.view.addSubview(self.previewImgView!)
			
			UIView.animateWithDuration(0.7,
				animations: { () -> Void in
					self.previewImgView?.alpha = 1.0
				},
				completion: { (finished:Bool) -> Void in
					if (finished)
					{
						self.closeBtn?.enabled = true
						self.closeBtn?.alpha = 0.0

						self.view.bringSubviewToFront(self.closeBtn!)
						UIView.animateWithDuration(0.1, animations: { () -> Void in
							self.closeBtn?.alpha = 1.0
						})
					}
				})
		}
	}
	
	// MARK: - CMMotionUtil Delegate methods
	
	func updateAttitude(pitch: Float, _ roll: Float, _ yaw: Float)
	{
	}
}

