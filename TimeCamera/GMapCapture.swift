//
//  GMapCapture.swift
//  TimeCamera
//
//  Created by 石郷 祐介 on 2015/05/24.
//  Copyright (c) 2015年 Yusk. All rights reserved.
//

import UIKit

protocol GMapCaptureDelegate: class
{
	func GMapCaptureDidCapture(gmapCapture:GMapCapture, image:UIImage)
}

class GMapCapture
{
	weak var delegate:GMapCaptureDelegate?
	
	private let baseURL = "http://maps.googleapis.com/maps/api/streetview"
	private var manager:AFHTTPSessionManager?
	
	func capture(latitude:Double, longitude:Double, heading:Double, pitch:Double)
	{
		self.manager = AFHTTPSessionManager()
		self.manager?.responseSerializer = AFImageResponseSerializer()
		
		var url = baseURL+"?"
		url += "size=568x320&"
		url += "location=\(latitude),\(longitude)&"
		url += "heading=\(heading)&"
		url += "pitch=\(pitch)&"
		url += "&fov=90&sensor=false"
		
		print(url)
		self.manager?.GET(
			url,
			parameters: nil,
			success: { (task:NSURLSessionDataTask!, responseObject:AnyObject!) -> Void in
				let stViewImg = responseObject as! UIImage
//				println(stViewImg.size)
				self.delegate?.GMapCaptureDidCapture(self, image: stViewImg)
			},
			failure: { (task:NSURLSessionDataTask!, error:NSError!) -> Void in
				print(error)
			})
	}
}
