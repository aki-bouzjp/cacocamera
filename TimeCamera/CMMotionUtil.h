//
//  CMMotionUtil.h
//  FlightLogger
//
//  Created by 桑原宜昭 on 2014/09/13.
//  Copyright (c) 2014年 桑原宜昭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

@protocol CMMotionUtilDelegate <NSObject>
- (void)updateAttitude:(float)pitch :(float)roll :(float)yaw;
@end

@interface CMMotionUtil : NSObject
@property (weak, nonatomic) id <CMMotionUtilDelegate> delegate;
@property (strong, nonatomic) NSUserDefaults *ud;
@property (strong, nonatomic) CMMotionManager *motionManager;
// iOSVersion
@property (nonatomic) float systemVersion;
// 姿勢値の初期値
@property (nonatomic) float defaultPitch;
@property (nonatomic) float defaultRoll;
@property (nonatomic) float defaultYaw;
// 姿勢値
@property (nonatomic) float pitch;
@property (nonatomic) float roll;
@property (nonatomic) float yaw;
// 初期値を取得するためのフラグ
@property (nonatomic) BOOL isResetAttitude;

// 初期化
- (id)initWithDelegate:(id <CMMotionUtilDelegate>)delegate;

// CMMotionのセットアップ
- (void)setupCMMotion;

// CMMotionの開始
- (void)startCMMotion;

// CMMotionの停止
- (void)stopCMMotion;

// 初期姿勢値の設定
- (void)resetAttitude;

// 姿勢値の設定
- (void)setAttitude:(float)pitch :(float)roll :(float)yaw;

@end
