//
//  LocationUtil.swift
//  TimeCamera
//
//  Created by 石郷 祐介 on 2015/05/24.
//  Copyright (c) 2015年 Yusk. All rights reserved.
//

import UIKit
import CoreLocation

class LocationUtil: NSObject, CLLocationManagerDelegate
{
	private var locationManager:CLLocationManager?
	var latitude:CLLocationDegrees = 0.0
	var longitude:CLLocationDegrees = 0.0
	var heading:CLLocationDirection = 0.0
	
	static var sharedInstance = LocationUtil()
	
	override private init()
	{
		super.init()
		
		self.locationManager = CLLocationManager()
		self.locationManager?.delegate = self
		self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
		self.locationManager?.distanceFilter = kCLDistanceFilterNone
	}
	
	func startLocationUpdating()
	{
		if (CLLocationManager.locationServicesEnabled())
		{
			let status = CLLocationManager.authorizationStatus()
			if (status == CLAuthorizationStatus.NotDetermined)
			{
				self.locationManager?.requestWhenInUseAuthorization()
			}
		}
	}
	
	private func setupUpdatingLocation()
	{
		self.locationManager?.startUpdatingLocation()
		self.locationManager?.startUpdatingHeading()
	}
	
	// MARK: - CLLocationManager delegate methods
	
	func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
	{
		self.latitude = manager.location.coordinate.latitude
		self.longitude = manager.location.coordinate.longitude
	}
	
	func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading)
	{
		self.heading = newHeading.trueHeading
	}
	
	func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
	{
		self.setupUpdatingLocation()
	}
	
	func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
	{
		print("error")
	}
}
